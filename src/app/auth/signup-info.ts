export class SignupInfo {

  username: string;
  role: string[];
  password: string;
  birthday: string;

  constructor(username: string, password: string, birthday: string) {
    this.username = username;
    this.role = ['user'];
    this.password = password;
    this.birthday = birthday;
  }
}
