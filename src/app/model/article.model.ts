import { Department } from './department.model';

export class Article {
  id: number;
  name: string;
  stock: number;
  department: Department;
}