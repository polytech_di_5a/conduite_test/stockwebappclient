import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Department } from '../model/department.model';
import { Article } from '../model/article.model';
import { Store } from '../model/store.model';
import { UserService } from '../services/user.service';
import { LoginService } from '../services/login.service';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  
  errorMessage: string;
  articleList: Article[];
  departmentList: Department[];
  storeList: Store[];
  username: string;

  constructor(private loginService: LoginService, private userService: UserService, private router: Router, private token: TokenStorageService) { this.username=token.getUsername()}

  ngOnInit() {
    //console.log(this.loginService);
    this.loginService.getUserPage().subscribe(
      data => {
        this.getStore();
        this.getDepartments();
        this.getArticles();
      },
      error => {
        this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
      }
    );
  }

  getArticles(): void {
      this.userService.getArticles()
      .subscribe(
        articleList => {
        var tmpArticleList = [];
        articleList.forEach((article) => {
          if(article.department && article.department.user &&article.department.user.username==this.username){
            tmpArticleList.push(article);
          }
          else if(article.department.store && article.department.store.user && article.department.store.user.username==this.username){
            tmpArticleList.push(article);
          }
        });
        this.articleList = tmpArticleList;
      });
  }

  addArticle(name: string, stock: number, department: Department): void {
    if(name.length>0 && department!==undefined && !isNaN(stock) && stock>=0){
      this.userService.addArticle({ name, stock, department } as Article)
        .subscribe(article => { 
          //console.log(article);
          this.articleList.push(article);},
          error1 => {},
          () => {},
        );
    }
  }

  updateArticleStock(stock: number, article: Article): void {
    if(!isNaN(stock) && stock>=0){
      article.stock = stock;
    }
    this.userService.updateArticle(article).subscribe();
  }

  updateArticle(name: string, article: Article): void {
    if(name.length>0){
      article.name = name;
    }
    this.userService.updateArticle(article).subscribe();
  }

  deleteArticle(article: Article): void {
    this.articleList = this.articleList.filter(l => l !== article);
    this.userService.deleteArticle(article).subscribe();
  }

  getDepartments(): void {
    this.userService.getDepartments()
      .subscribe(departmentList => {
        var tmpDepartmentList = [];
        departmentList.forEach((department) => {
          if(department.user && department.user.username==this.username){
            tmpDepartmentList.push(department);
          }
          else if(department.store && department.store.user && department.store.user.username==this.username){
            tmpDepartmentList.push(department);
          }
        });
        this.departmentList = tmpDepartmentList;
      });
  }
  
  addDepartment(name: string, store: Store): void {
    name = name.trim();
    if(name.length>0){
    this.userService.addDepartment({ name, store } as Department)
      .subscribe(department => { this.departmentList.push(department); },
        error1 => {},
        () => {},
      );
    }
  }

  updateDepartmentContent(name: string, department: Department): void {
    if(name.length>0){
      department.name = name;
    }
    this.userService.updateDepartment(department).subscribe();
  }

  deleteDepartment(department: Department): void {
    let bool:Boolean=true;
    for(let article in this.articleList){
      if(this.articleList[article].department.id==department.id){
        bool=false;
        break;
      }
    }
    if(bool){
      this.departmentList = this.departmentList.filter(l => l !== department);
      this.userService.deleteDepartment(department).subscribe();
    }
  }

  getStore(): void {
    this.userService.getStores()
      .subscribe(storeList => {
        var tmpStoreList = [];
        storeList.forEach((store) => {
          if(store.user && store.user.username==this.username){
            tmpStoreList.push(store);
          }
        });
        this.storeList = tmpStoreList;
      });
  }
}
