import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Article } from '../model/article.model';
import { Department } from '../model/department.model';
import { Store } from '../model/store.model';
import { User } from '../model/user.model';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private articlesUrl = 'http://localhost:8080/restApi/articles';
  private departmentsUrl = 'http://localhost:8080/restApi/departments';
  private storesUrl = 'http://localhost:8080/restApi/stores';
  private usersUrl = 'http://localhost:8080/restApi/auth';

  constructor(private http: HttpClient) { }

  /*------------------------*/
  /*---------USERS---------*/
  /*------------------------*/

  /** GET articles from the server */
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }


  /*------------------------*/
  /*---------GRADES---------*/
  /*------------------------*/

  /** GET articles from the server */
  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.articlesUrl);
  }

  /** GET article by id. Will 404 if id not found */
  getArticle(id: number): Observable<Article> {
    const url = `${this.articlesUrl}/${id}`;
    return this.http.get<Article>(url).pipe(
      tap(_ => this.log(`fetched article id=${id}`)),
      catchError(this.handleError<Article>(`getArticle id=${id}`))
    );
  }

  /** POST: add a new article to the server */
  addArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(this.articlesUrl, article, httpOptions).pipe(
      tap((articleAdded: Article) => this.log(`added article id=${articleAdded.id}`)),
      catchError(this.handleError<Article>('addArticle'))
    );
  }

  /** DELETE: delete the article from the server */
  deleteArticle(article: Article | number): Observable<Article> {
    const id = typeof article === 'number' ? article : article.id;
    const url = `${this.articlesUrl}/${id}`;
    return this.http.delete<Article>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted article id=${id}`)),
      catchError(this.handleError<Article>('deleteArticle'))
    );
  }

  /** PUT: update the article on the server */
  updateArticle(article: Article): Observable<any> {
    const id = typeof article === 'number' ? article : article.id;
    const url = `${this.articlesUrl}/${id}`;
    return this.http.put(url, article, httpOptions).pipe(
      tap(_ => this.log(`updated article id=${article.id}`)),
      catchError(this.handleError<any>('updateArticle'))
    );
  }



  /*------------------------*/
  /*---------Departments---------*/
  /*------------------------*/
  
  /** GET departments from the server */
  getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(this.departmentsUrl);
  }

  /** POST: add a new department to the server */
  addDepartment(department: Department): Observable<Department> {
    return this.http.post<Department>(this.departmentsUrl, department, httpOptions).pipe(
      tap((departmentAdded: Department) => this.log(`added department id=${departmentAdded.id}`)),
      catchError(this.handleError<Department>('addDepartment'))
    );
  }

  /** GET department by id. Will 404 if id not found */
  getDepartment(id: number): Observable<Department> {
    const url = `${this.departmentsUrl}/${id}`;
    return this.http.get<Department>(url).pipe(
      tap(_ => this.log(`fetched department id=${id}`)),
      catchError(this.handleError<Department>(`getDepartment id=${id}`))
    );
  }

  /** DELETE: delete the department from the server */
  deleteDepartment(department: Department | number): Observable<Department> {
    const id = typeof department === 'number' ? department : department.id;
    const url = `${this.departmentsUrl}/${id}`;
    return this.http.delete<Department>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted department id=${id}`)),
      catchError(this.handleError<Department>('deleteDepartment'))
    );
  }

  /** PUT: update the department on the server */
  updateDepartment(department: Department): Observable<any> {
    const id = typeof department === 'number' ? department : department.id;
    const url = `${this.departmentsUrl}/${id}`;
    return this.http.put(url, department, httpOptions).pipe(
      tap(_ => this.log(`updated department id=${department.id}`)),
      catchError(this.handleError<any>('updateDepartment'))
    );
  }


  /*------------------------*/
  /*---------Store---------*/
  /*------------------------*/

  /** GET stores from the server */
  getStores(): Observable<Store[]> {
    return this.http.get<Store[]>(this.storesUrl);
  }
  
  /** GET store by id. Will 404 if id not found */
  getStore(id: number): Observable<Store> {
    const url = `${this.storesUrl}/${id}`;
    return this.http.get<Store>(url).pipe(
      tap(_ => this.log(`fetched store id=${id}`)),
      catchError(this.handleError<Store>(`getStore id=${id}`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a ArticleService message with the MessageService */
  private log(message: string) {
    console.log('ArticleService: ' + message);
  }
}
